export function convertRgb(rgb: string): [number, number, number] {
    const match = rgb.match(
        /^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/
    );
    if (match) {
        const [red, green, blue] = match;
        return [parseInt(red, 10), parseInt(green, 10), parseInt(blue, 10)];
    } else {
        return [0, 0, 0];
    }
}

export function convertHex(hex: string): [number, number, number] {
    const n = +('0x' + hex.slice(1));
    const red = n >> 16;
    const green = (n >> 8) & 255;
    const blue = n & 255;
    return [red, green, blue];
}

export function isDark(color: string): boolean {
    let red;
    let green;
    let blue;
    if (color.match(/^rgb/)) {
        [red, green, blue] = convertRgb(color);
    } else {
        [red, green, blue] = convertHex(color);
    }
    const hsp = Math.sqrt(
        0.299 * (red * red) + 0.587 * (green * green) + 0.114 * (blue * blue)
    );
    return hsp < 127.5 ? true : false;
}
