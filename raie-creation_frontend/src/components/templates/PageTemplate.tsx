import React, { ReactNode } from 'react';
import { makeStyles, Container } from '@material-ui/core';

export interface PageTemplateProps {
    children: ReactNode;
    header?: ReactNode;
    nav?: ReactNode;
}

const useStyles = makeStyles(theme => ({
    root: {
        minHeight: '100vh'
    }
}));

export function PageTemplate({ children, header, nav }: PageTemplateProps) {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            {header && <header>{header}</header>}
            {nav && nav}
            <main>
                <Container>{children}</Container>
            </main>
        </div>
    );
}
