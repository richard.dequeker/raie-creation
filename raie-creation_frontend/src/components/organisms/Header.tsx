import React from 'react';
import {
    AppBar,
    Toolbar,
    IconButton,
    Typography,
    makeStyles
} from '@material-ui/core';

import MenuIcon from '@material-ui/icons/menu';

export interface HeaderProps {
    onMenuClick: () => void;
}

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    title: {
        flexGrow: 1
    }
}));

export function Header({ onMenuClick }: HeaderProps) {
    const classes = useStyles();
    return (
        <AppBar className={classes.root} position="static">
            <Toolbar>
                <IconButton
                    className={classes.menuButton}
                    edge="start"
                    color="inherit"
                    aria-label="menu"
                    onClick={onMenuClick}
                >
                    <MenuIcon />
                </IconButton>
                <Typography className={classes.title} variant="h6">
                    Raie-création
                </Typography>
            </Toolbar>
        </AppBar>
    );
}
