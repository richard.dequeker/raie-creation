import React from 'react';
import {
    Drawer,
    List,
    ListItem,
    ListItemText,
    Divider,
    Icon,
    makeStyles
} from '@material-ui/core';
import { Link } from 'react-router-dom';

import { LinkProps } from 'src/routes/types';

export interface SidenavProps {
    open: boolean;
    onClose: () => void;
    links: LinkProps[];
}

const useStyles = makeStyles({
    list: {
        width: 250
    },
    link: {
        textDecoration: 'none'
    }
});

export function Sidenav({ open, onClose, links }: SidenavProps) {
    const classes = useStyles();
    return (
        <Drawer open={open} onClose={onClose}>
            <div className={classes.list}>
                <Divider />
                <List>
                    {links.map((link: LinkProps, index) => (
                        <Link
                            className={classes.link}
                            to={link.to}
                            key={`link-${index}`}
                        >
                            <ListItem button>
                                <ListItemText primary={link.name} />
                                {link.icon && <Icon>{link.icon}</Icon>}
                            </ListItem>
                        </Link>
                    ))}
                </List>
                <Divider />
            </div>
        </Drawer>
    );
}
