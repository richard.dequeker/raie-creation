import { useMemo } from 'react';

export const useCapitalize = (value: string) => {
    return useMemo(() => {
        const splitted = value.split('');
        splitted[0] = splitted[0].toUpperCase();
        return splitted.join('');
    }, [value]);
};
