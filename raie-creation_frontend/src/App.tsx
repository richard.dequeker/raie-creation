import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import { PublicRoutes } from './routes/PublicRoutes';

export const App = () => {
    return (
        <Router>
            <PublicRoutes />
        </Router>
    );
};
