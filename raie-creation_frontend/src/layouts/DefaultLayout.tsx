import React, { useState, ReactNode } from 'react';

import { LinkProps } from 'src/routes/types';
import { PageTemplate } from 'src/components/templates';
import { Header, Sidenav } from 'src/components/organisms';

export interface DefaultLayoutProps {
    links: LinkProps[];
    children: ReactNode;
}

export function DefaultLayout({ children, links }: DefaultLayoutProps) {
    const [isSidenavOpen, setSidenavOpen] = useState(false);
    const toggleSidenav = () => {
        setSidenavOpen(!isSidenavOpen);
    };
    const onSidenavClose = () => {
        setSidenavOpen(false);
    };
    return (
        <PageTemplate
            header={<Header onMenuClick={toggleSidenav} />}
            nav={
                <Sidenav
                    open={isSidenavOpen}
                    onClose={onSidenavClose}
                    links={links}
                />
            }
        >
            {children}
        </PageTemplate>
    );
}
