import React, { lazy, Suspense } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { LinkProps } from './types';
import { DefaultLayout } from 'src/layouts/DefaultLayout';

const HomePage = lazy(() => import('../pages/Home'));
const SignInPage = lazy(() => import('../pages/SignIn'));
const SignUpPage = lazy(() => import('../pages/SignUp'));
const AppointmentPage = lazy(() => import('../pages/Appointment'));
const ContactPage = lazy(() => import('../pages/Contact'));

const publicLinks: LinkProps[] = [
    {
        name: 'Home',
        to: '/'
    },
    {
        name: 'Contact',
        to: '/contact'
    },
    {
        name: 'Appointment',
        to: '/appointment'
    },
    {
        name: 'Sign in',
        to: '/signin'
    },
    {
        name: 'Sign up',
        to: '/signup'
    }
];

export const PublicRoutes = () => (
    <Switch>
        <DefaultLayout links={publicLinks}>
            <Suspense fallback={'loading...'}>
                <Route path="/" component={HomePage} exact={true} />
                <Route path="/appointment" component={AppointmentPage} />
                <Route path="/signin" component={SignInPage} />
                <Route path="/signup" component={SignUpPage} />
                <Route path="/contact" component={ContactPage} />
            </Suspense>
            <Redirect to="/contact" />
        </DefaultLayout>
    </Switch>
);
