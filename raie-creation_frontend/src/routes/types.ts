export interface LinkProps {
    name: string;
    to: string;
    icon?: string;
}
