import create from 'zustand';
import { AuthPayload, Credentials } from 'src/types';
import { Store } from './types';
import { API_URL } from 'src/constants';

interface Appointment {
    id?: string;
    date: string;
}

interface GuestState {
    isLoading: boolean;
    appointments: Appointment[];
    error?: any;
}

interface GuestActions {
    loadAppointments(): Promise<void>;
    createAppointment(date: string): Promise<void>;
}

type GuestStore = Store<GuestState, GuestActions>;

const headers = {
    'Content-type': 'application/json'
};

export const [useGuestStore] = create<GuestStore>((set, get) => ({
    isLoading: false,
    appointments: [],
    actions: {
        loadAppointments: async () => {
            set({ isLoading: true });
            try {
                const response = await fetch(API_URL + '/appointment');
                const appointments = await response.json();
                console.log(appointments);
                set({ appointments });
            } catch (error) {
                set({ error });
            } finally {
                set({ isLoading: false });
            }
        },
        createAppointment: async (date: string) => {
            console.log(date);
            set({ isLoading: true });
            try {
                const response = await fetch(API_URL + '/appointment', {
                    method: 'POST',
                    body: JSON.stringify({ date }),
                    headers
                });
                const appointment = await response.json();
                const { appointments } = get();
                set({ appointments: [...appointments, appointment] });
            } catch (error) {
                set({ error });
            } finally {
                set({ isLoading: false });
            }
        }
    }
}));
