import create from 'zustand';
import { AuthPayload, Credentials } from 'src/types';
import { Store } from './types';
import { API_URL } from 'src/constants';

interface AuthState {
    isLoading: boolean;
    auth: AuthPayload | null;
    error?: any;
}

interface AuthActions {
    signIn(credentials: Credentials): void;
    signUp(credentials: Credentials): void;
}

type AuthStore = Store<AuthState, AuthActions>;

const headers = {
    'Content-type': 'application/json'
};

export const [useAuthStore] = create<AuthStore>((set, get) => ({
    isLoading: false,
    auth: null,
    actions: {
        signIn: async (credentials: Credentials) => {
            set({ isLoading: true });
            try {
                const response = await fetch(API_URL + '/auth/signin', {
                    method: 'POST',
                    body: JSON.stringify(credentials),
                    headers
                });
                const res = await response.json();
                console.log(res);
            } catch (error) {
                set({ error });
            } finally {
                set({ isLoading: false });
            }
        },
        signUp: async (credentials: Credentials) => {
            set({ isLoading: true });
            try {
                const response = await fetch(API_URL + '/auth/signup', {
                    method: 'POST',
                    body: JSON.stringify(credentials),
                    headers
                });
                const res = await response.json();
                console.log(res);
            } catch (error) {
                set({ error });
            } finally {
                set({ isLoading: false });
            }
        }
    }
}));
