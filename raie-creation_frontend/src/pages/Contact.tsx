import React, { useRef, useEffect, useState } from 'react';
import { makeStyles, Card, Button } from '@material-ui/core';
import FullCalendar from '@fullcalendar/react';
import { EventInput } from '@fullcalendar/core';
import timeGridPlugin from '@fullcalendar/timegrid';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';

import { useGuestStore } from '../stores/guest.store';

const useStyles = makeStyles({
    root: {
        textAlign: 'center'
    },
    card: {
        margin: '2em',
        padding: '2em'
    }
});

interface Event {
    title: string;
    start?: string | number | Date | number[];
}

const Contact = () => {
    const classes = useStyles();

    const { actions, appointments } = useGuestStore();

    const calendarRef = useRef(null);

    const [currentEvent, setCurrentEvent] = useState<Event>({
        title: 'Votre rendez vous',
        start: new Date()
    });
    const [events, setEvents] = useState<Event[]>([currentEvent]);

    useEffect(() => {
        actions.loadAppointments();
    }, []);

    useEffect(() => {
        setEvents([
            ...events,
            ...appointments.map((ap, index) => ({
                start: new Date(ap.date),
                title: index.toString()
            }))
        ]);
    }, [appointments]);

    useEffect(() => {
        setEvents([...events, currentEvent]);
    }, [currentEvent]);

    const handleDateClick = ({ date }: EventInput) => {
        setCurrentEvent({ title: 'Votre rendez vous', start: date });
    };

    const handleConfirmClick = () => {
        if (currentEvent.start) {
            actions.createAppointment(currentEvent.start as string);
        }
    };

    return (
        <div className={classes.root}>
            <Card className={classes.card}>
                <FullCalendar
                    defaultView="timeGridDay"
                    header={{
                        left: 'prev,next today',
                        center: 'title'
                    }}
                    plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                    ref={calendarRef}
                    events={events}
                    dateClick={handleDateClick}
                    businessHours={{
                        daysOfWeek: [1, 2, 3, 4, 5, 6],
                        startTime: '09:00',
                        endTime: '18:00'
                    }}
                    selectConstraint={{
                        daysOfWeek: [1, 2, 3, 4, 5, 6],
                        startTime: '09:00',
                        endTime: '18:00'
                    }}
                    eventConstraint={{
                        daysOfWeek: [1, 2, 3, 4, 5, 6],
                        startTime: '09:00',
                        endTime: '18:00'
                    }}
                />
                <Button onClick={handleConfirmClick}>Confirmer</Button>
            </Card>
        </div>
    );
};

export default Contact;
