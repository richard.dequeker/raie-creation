import React, { useRef, useEffect, useState } from 'react';
import { makeStyles, Card } from '@material-ui/core';
import FullCalendar from '@fullcalendar/react';
import timeGridPlugin from '@fullcalendar/timegrid';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';

import { useGuestStore } from '../stores/guest.store';

const useStyles = makeStyles({
    root: {
        textAlign: 'center'
    },
    card: {
        margin: '2em',
        padding: '2em'
    }
});

interface Event {
    title: string;
    start?: string | number | Date | number[];
}

const Appointment = () => {
    const classes = useStyles();

    const { actions, appointments } = useGuestStore();

    const calendarRef = useRef(null);

    const [events, setEvents] = useState<Event[]>([]);

    useEffect(() => {
        actions.loadAppointments();
    }, []);

    useEffect(() => {
        console.log(appointments);
        setEvents([
            ...appointments.map((ap, index) => ({
                start: new Date(ap.date),
                title: index.toString()
            }))
        ]);
    }, [appointments]);

    return (
        <div className={classes.root}>
            <Card className={classes.card}>
                <FullCalendar
                    defaultView="timeGridWeek"
                    header={{
                        left: 'prev,next today',
                        center: 'title',
                        right: 'timeGridWeek,timeGridDay'
                    }}
                    plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                    ref={calendarRef}
                    events={events}
                    businessHours={{
                        daysOfWeek: [1, 2, 3, 4, 5, 6],
                        startTime: '09:00',
                        endTime: '18:00'
                    }}
                    selectConstraint={{
                        daysOfWeek: [1, 2, 3, 4, 5, 6],
                        startTime: '09:00',
                        endTime: '18:00'
                    }}
                    eventConstraint={{
                        daysOfWeek: [1, 2, 3, 4, 5, 6],
                        startTime: '09:00',
                        endTime: '18:00'
                    }}
                />
            </Card>
        </div>
    );
};

export default Appointment;
