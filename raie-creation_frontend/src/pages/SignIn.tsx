import React from 'react';
import { TextField, Card, makeStyles, Button } from '@material-ui/core';

const useStyles = makeStyles({
    root: {
        textAlign: 'center'
    },
    card: {
        margin: '2em',
        padding: '2em'
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    field: {
        marginBottom: '1em'
    }
});

const SignIn = () => {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Card className={classes.card}>
                <form className={classes.form}>
                    <TextField
                        className={classes.field}
                        type="email"
                        variant="outlined"
                    />
                    <TextField
                        className={classes.field}
                        type="password"
                        variant="outlined"
                    />
                    <Button>Sign in</Button>
                </form>
            </Card>
        </div>
    );
};

export default SignIn;
