export interface User {
    id?: string;
    email: string;
}

export interface AuthPayload {
    user: User;
    token: string;
}

export interface Credentials {
    email: string;
    password: string;
}
