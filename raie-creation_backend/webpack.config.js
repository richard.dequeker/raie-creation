const NodemonPlugin = require('nodemon-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const TsConfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const path = require('path');

const ROOT_DIRECTORY = process.cwd();

module.exports = {
    mode: 'development',
    target: 'node',
    watch: true,
    externals: [nodeExternals()],
    entry: './src',
    output: {
        path: path.resolve(ROOT_DIRECTORY, 'dist'),
        filename: 'server.bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'awesome-typescript-loader'
            }
        ]
    },
    plugins: [new NodemonPlugin()],
    resolve: {
        plugins: [new TsConfigPathsPlugin()],
        extensions: ['.ts', '.js']
    }
};
