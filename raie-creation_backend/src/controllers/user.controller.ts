import { Controller, Get } from '@tsed/common';

import { UserService } from 'src/services';

@Controller('/user')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Get('/')
    async find() {
        return await this.userService.find();
    }
}
