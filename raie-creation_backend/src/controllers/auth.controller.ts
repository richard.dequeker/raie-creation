import { Controller, Post, BodyParams } from '@tsed/common';

import { AuthService } from 'src/services';
import { Credentials, UserCreation } from 'src/models';

@Controller('/auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post('/signin')
    async signIn(@BodyParams() credentials: Credentials) {
        return await this.authService.signIn(credentials);
    }
    @Post('/signup')
    async signUn(@BodyParams() userData: UserCreation) {
        return await this.authService.signIn(userData);
    }
}
