import { Controller, Get, Post, BodyParams } from '@tsed/common';

import { AppointmentService } from 'src/services';

@Controller('/appointment')
export class AppointmentController {
    constructor(private readonly appointmentService: AppointmentService) {}

    @Get('/')
    async find() {
        return await this.appointmentService.find();
    }

    @Post('/')
    async create(@BodyParams() { date }: { date: string }) {
        return await this.appointmentService.create(date);
    }
}
