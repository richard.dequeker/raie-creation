import { Entity, Column, OneToMany } from 'typeorm';
import crypto from 'crypto';

import { EntityBase } from '../entity-base';
import { Role, UserData } from './types';
import { Appointment } from '../appointment';

@Entity('user')
export class User extends EntityBase {
    @Column({ unique: true })
    email!: string;

    @Column({ type: 'simple-array', default: [Role.User] })
    roles!: Role[];

    @Column({ nullable: true })
    firstName?: string;

    @Column({ nullable: true })
    lastName?: string;

    @Column()
    private salt!: string;

    @Column()
    private hash!: string;

    set password(password: string) {
        this.salt = crypto.randomBytes(16).toString('hex');
        this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 64, 'sha512').toString('hex');
    }

    toData(): UserData {
        const data: Partial<User> = JSON.parse(JSON.stringify(this));
        delete data.password;
        return data as UserData;
    }

    comparePassword(password: string): boolean {
        const hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
        return this.hash === hash;
    }
}
