export enum Role {
    User = 'USER',
    Admin = 'ADMIN',
    Maintainer = 'MAINTAINER'
}

export interface CreateUser {
    email: string;
    password: string;
}

export interface UserData {
    id: string;
    email: string;
    roles: Role[];
    firstName?: string;
    lastName?: string;
}

export interface JwtPayload {
    id: string;
    email: string;
    roles: Role[];
}

export interface Tokens {
    accessToken: string;
    refreshToken: string;
}

export type AuthData = UserData & Tokens;
