import { PrimaryGeneratedColumn, BeforeInsert } from 'typeorm';
import uuid from 'uuid/v4';

export class EntityBase {
    @PrimaryGeneratedColumn()
    id!: string;

    @BeforeInsert()
    private generateId() {
        this.id = uuid();
    }
}
