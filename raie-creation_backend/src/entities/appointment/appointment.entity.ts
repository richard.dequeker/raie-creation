import { Entity, Column } from 'typeorm';

import { EntityBase } from '../entity-base';

@Entity('appointment')
export class Appointment extends EntityBase {
    // TODO to date
    @Column()
    date!: string;
}
