import { Service } from '@tsed/di';

import { User } from 'src/entities';
import { EntityService } from './entity-service';

@Service()
export class UserService extends EntityService {
    async find() {
        return await this.manager.find(User);
    }

    async findOne(where: Partial<User>) {
        return await this.manager.findOne(User, { where });
    }

    async create(email: string, password: string) {
        const user = new User();
        user.email = email;
        user.password = password;
        return await this.manager.save(user);
    }
}
