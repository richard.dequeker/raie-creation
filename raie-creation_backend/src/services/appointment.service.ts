import { InjectRepository } from 'typeorm-typedi-extensions';
import { Service } from '@tsed/di';

import { Appointment } from 'src/entities';
import { Repository, getRepository } from 'typeorm';
import { EntityService } from './entity-service';

@Service()
export class AppointmentService extends EntityService {
    async find() {
        return await this.manager.find(Appointment);
    }

    async create(date: string) {
        console.log(date);
        const appointment = new Appointment();
        appointment.date = date;
        return await this.manager.save(appointment);
    }
}
