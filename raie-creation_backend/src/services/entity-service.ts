import { Service } from '@tsed/di';

import { Repository, getRepository, EntityManager } from 'typeorm';
import { TypeORMService } from '@tsed/typeorm';

@Service()
export class EntityService {
    protected manager!: ReturnType<TypeORMService['get']>['manager'];

    constructor(private typeORMService: TypeORMService) {}

    $afterRoutesInit() {
        this.manager = this.typeORMService.get().manager;
    }
}
