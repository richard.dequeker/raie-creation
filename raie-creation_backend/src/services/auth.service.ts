import { Service } from '@tsed/di';
import { Unauthorized, Conflict } from 'ts-httpexceptions';
import { sign, verify } from 'jsonwebtoken';

import { UserService } from './user.service';
import { User } from 'src/entities';
import { securityConfig } from 'src/utils/config';
import { Credentials, UserCreation } from 'src/models';

@Service()
export class AuthService {
    constructor(private readonly userService: UserService) {}

    async signIn({ email, password }: Credentials) {
        const user = await this.userService.findOne({ email });
        if (!user) {
            throw new Unauthorized('Unauthorized access');
        }
        if (!user.comparePassword(password)) {
            throw new Unauthorized('Unauthorized access');
        }
        const token = this.generateToken(user);
        return { user: user.toData(), token };
    }

    async signUp({ email, password }: UserCreation) {
        const userExist = await this.userService.findOne({ email });
        if (userExist) {
            throw new Conflict(`User with email: ${email} already exist`);
        }
        await this.userService.create(email, password);
    }

    private generateToken(user: User) {
        return sign({ id: user.id }, securityConfig.jwt.secret, {
            expiresIn: securityConfig.jwt.accessExpireIn
        });
    }

    private verifyToken(token: string) {
        return verify(token, securityConfig.jwt.secret);
    }
}
