import { Request, Response, NextFunction, ErrorRequestHandler } from 'express';

export const errorMiddleware: ErrorRequestHandler = (err, req, res, next) => {
    if (err) {
        switch (err.name) {
            case 'UnauthorizedError':
                res.status(401).send();
            case 'UnprocessableEntityError':
                res.status(422).send();
            case 'ConflictError':
                res.status(409).send();
            case 'EntityNotFoundError':
                res.status(404).send();
            default:
                res.status(500).send();
        }
        next(err);
    }
};
