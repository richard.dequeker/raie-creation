import { json, urlencoded } from 'express';
import {
    GlobalAcceptMimesMiddleware,
    ServerLoader,
    ServerSettings,
    IDIConfigurationOptions
} from '@tsed/common';
import '@tsed/swagger';
import cors from 'cors';

import { apiConfig, databaseConfig } from './utils/config';
import { UserController, AppointmentController, AuthController } from './controllers';
import { User, Appointment } from './entities';

const rootDir = __dirname;

@ServerSettings({
    rootDir,
    acceptMimes: ['application/json'],
    httpPort: apiConfig.port,
    httpsPort: false,
    logger: {
        logRequest: true
    },
    mount: {
        '/api/v1': [UserController, AuthController, AppointmentController]
    },
    swagger: [
        {
            path: '/api-docs'
        }
    ],
    typeorm: [
        {
            name: 'default',
            type: 'postgres',
            database: databaseConfig.name,
            username: databaseConfig.username,
            password: databaseConfig.password,
            host: databaseConfig.host,
            port: databaseConfig.port,
            synchronize: true,
            entities: [User, Appointment]
        }
    ]
})
export class Server extends ServerLoader {
    constructor(settings?: IDIConfigurationOptions) {
        super(settings);
    }

    $beforeRoutesInit(): void | Promise<any> {
        this.use(GlobalAcceptMimesMiddleware)
            .use(cors({ origin: '*' }))
            .use(json())
            .use(urlencoded({ extended: true }));
        return;
    }
}
