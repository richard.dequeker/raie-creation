import config from 'config';

export interface ApiConfig {
    port: number;
}

export const apiConfig: ApiConfig = config.get('api');

export interface SecurityConfig {
    cors: {
        origin: string;
    };
    jwt: {
        secret: string;
        accessExpireIn: string;
        refreshExpireIn: string;
    };
}

export const securityConfig: SecurityConfig = config.get('security');

export interface DatabaseConfig {
    port: number;
    host: string;
    name: string;
    username: string;
    password: string;
}

export const databaseConfig: DatabaseConfig = config.get('database');
